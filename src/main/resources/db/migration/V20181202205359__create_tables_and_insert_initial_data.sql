CREATE TABLE customer
(
  Customer_ID INTEGER not null
    constraint customer_pk
      primary key,
  FirstName VARCHAR(50),
  LastName varchar(50),
  OrganizationName VARCHAR(255),
  Phone VARCHAR(26),
  Email VARCHAR(255),
  StreetAddress VARCHAR(100),
  ZipCode VARCHAR(12),
  City VARCHAR(60),
  State VARCHAR(55),
  Country VARCHAR(55)

);


CREATE TABLE employee
(
  Employee_ID INTEGER not null
    constraint employee_pk
      primary key,
  FirstName VARCHAR(50),
  LastName VARCHAR(50),
  EmployeeTitle VARCHAR(50),
  Phone VARCHAR(26),
  Email VARCHAR(55),
  StreetAddress VARCHAR(100),
  ZipCode VARCHAR(12),
  City VARCHAR(60),
  State VARCHAR(55),
  Country VARCHAR(55)

);


CREATE TABLE component
(
  Component_ID INTEGER not null
    constraint component_pk
      primary key,
  ComponentName VARCHAR(50),
  ComponentType VARCHAR(50),
  ComponentPrice DECIMAL(18,2)

);


CREATE TABLE "order"
(
  Order_ID INTEGER not null
    constraint order_pk
      primary key,
  DateOrdered VARCHAR(50),
  OrderStatus VARCHAR(50),
  QuantityOrdered INTEGER,
  Subtotal DECIMAL(18,2),
  LaborCharge DECIMAL(18,2),
  Tax DECIMAL(18,2),
  Total DECIMAL(18,2),
  Customer_ID INTEGER
    constraint order_Customer_ID_fk
      references customer,
  Employee_ID INTEGER
    constraint order_Employee_ID_fk
      references employee
);


CREATE TABLE product
(
  Product_ID INTEGER not null
    constraint product_pk
      primary key,
  ProductName VARCHAR(50),
  ProductPrice DECIMAL(18,2),
  Order_ID INTEGER
    constraint product_Order_ID_fk
      references "order"
);


CREATE TABLE productcomponent
(
  ProductComponent_ID INTEGER not null
    constraint productcomponent_pk
      primary key,
  Quantity INTEGER,
  Product_ID INTEGER
    constraint productcomponent_Product_ID_fk
      references product,
  Component_ID INTEGER
    constraint productcomponent_Component_ID_fk
      references component
);


INSERT INTO "public"."employee" ("employee_id", "firstname", "lastname", "employeetitle", "phone", "email", "streetaddress", "zipcode", "city", "state", "country") VALUES (1, 'John', 'Smith', 'Shop Owner', '832-554-6455', 'wonderbakery_JohnSmith@gmail.com', '1000 University Drive', '77002', 'Houston', 'Texas', 'USA');

INSERT INTO "public"."employee" ("employee_id", "firstname", "lastname", "employeetitle", "phone", "email", "streetaddress", "zipcode", "city", "state", "country") VALUES (2, 'Jane', 'Doe', 'Shop Manager', '281-456-7410', 'wonderbakery_JaneDoe@gmail.com', '1234 Buckey Street', '77589', 'Houston', 'Texas', 'USA');

INSERT INTO "public"."employee" ("employee_id", "firstname", "lastname", "employeetitle", "phone", "email", "streetaddress", "zipcode", "city", "state", "country") VALUES (3, 'Andy', 'Lee', 'Worker', '714-789-5546', 'wonderbakery_AndyLee@gmail.com', '9123 Cougar Boulevard', '77025', 'Houston', 'Texas', 'USA');

INSERT INTO "public"."employee" ("employee_id", "firstname", "lastname", "employeetitle", "phone", "email", "streetaddress", "zipcode", "city", "state", "country") VALUES (4, 'Sandy', 'Lee', 'Worker', '619-579-8915', 'wonderbakery_SandyLee@gmail.com', '9213 Cougar Boulevard', '77025', 'Houston', 'Texas', 'USA');

INSERT INTO "public"."customer" ("customer_id", "firstname", "lastname", "organizationname", "phone", "email", "streetaddress", "zipcode", "city", "state", "country") VALUES (1, 'Dat', 'Nguyen', 'UH Student', '832-546-6789', 'dn_cis3368@gmail.com', '9876 Bingle Drive', '77012', 'Houston', 'Texas', 'USA');

INSERT INTO "public"."customer" ("customer_id", "firstname", "lastname", "organizationname", "phone", "email", "streetaddress", "zipcode", "city", "state", "country") VALUES (2, 'Lanny', 'Lee', 'UH Student', '281-123-4567', 'll_cis3368@gmail.com', '1237 Churchill Boulevard', '77458', 'Houston', 'Texas', 'USA');

INSERT INTO "public"."component" ("component_id", "componentname", "componenttype", "componentprice") VALUES (1, 'Chocolate Frosting', 'Frosting', 2.00);

INSERT INTO "public"."component" ("component_id", "componentname", "componenttype", "componentprice") VALUES (2, 'Strawberry Frosting', 'Frosting', 2.00);

INSERT INTO "public"."component" ("component_id", "componentname", "componenttype", "componentprice") VALUES (3, 'Figure', 'Ornament', 1.00);

INSERT INTO "public"."component" ("component_id", "componentname", "componenttype", "componentprice") VALUES (4, 'Candle', 'Ornament', 1.00);

INSERT INTO "public"."order" ("order_id", "dateordered", "orderstatus", "quantityordered", "subtotal", "laborcharge", "tax", "total", "customer_id", "employee_id") VALUES (1, '2018-09-22', 'Completed', 1, 7.00, 0.10, 0.034, 7.938, 1, 3);

INSERT INTO "public"."order" ("order_id", "dateordered", "orderstatus", "quantityordered", "subtotal", "laborcharge", "tax", "total", "customer_id", "employee_id") VALUES (2, '2018-12-02', 'Pending', 1, 7.00, 0.10, 0.034, 7.938, 2, 4);

INSERT INTO "public"."product" ("product_id", "productname", "productprice", "order_id") VALUES (1, 'Chocolate Cake', 4.00, 1);

INSERT INTO "public"."product" ("product_id", "productname", "productprice", "order_id") VALUES (2, 'Strawberry Cake', 4.00, 2);

INSERT INTO "public"."productcomponent" ("productcomponent_id", "quantity", "product_id", "component_id") VALUES (1, 1, 1, 1);

INSERT INTO "public"."productcomponent" ("productcomponent_id", "quantity", "product_id", "component_id") VALUES (2, 1, 1, 3);

INSERT INTO "public"."productcomponent" ("productcomponent_id", "quantity", "product_id", "component_id") VALUES (3, 1, 2, 2);

INSERT INTO "public"."productcomponent" ("productcomponent_id", "quantity", "product_id", "component_id") VALUES (4, 1, 2, 4);