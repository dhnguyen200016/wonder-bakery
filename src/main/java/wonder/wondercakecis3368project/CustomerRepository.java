package wonder.wondercakecis3368project;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends CrudRepository<Customer,Integer> {

    //Customer findByFirstname(String CustFirstName);

}
