package wonder.wondercakecis3368project;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
public class Product implements Serializable {
    private int productId;
    private String productname;
    private BigDecimal productprice;

    private Order order;    //M-1 - Product to Order
    private Set<Productcomponent> productcomponents = new HashSet<>();  //1-M - Product to Productcomponent
    //////////////////////////////////////
    public Product() {

    }

    /////////////////////////////////////
    //M-1 - Product to Order
    @ManyToOne(cascade = CascadeType.PERSIST)
    public Order getOrder(){
        return order;}

    public void setOrder(Order order){
        this.order = order;
    }

    /////////////////////////////////////
    //1-M - Product to Productcomponent
    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    public Set<Productcomponent> getProductcomponents(){return productcomponents;}

    public void setProductcomponents(Set<Productcomponent> productcomponents) {
        this.productcomponents = productcomponents;
    }

    /////////////////////////////////////
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "product_id", nullable = false)
    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    @Basic
    @Column(name = "productname", nullable = true, length = 50)
    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }

    @Basic
    @Column(name = "productprice", nullable = true, precision = 2)
    public BigDecimal getProductprice() {
        return productprice;
    }

    public void setProductprice(BigDecimal productprice) {
        this.productprice = productprice;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return productId == product.productId &&
                Objects.equals(productname, product.productname) &&
                Objects.equals(productprice, product.productprice);
    }

    @Override
    public int hashCode() {
        return Objects.hash(productId, productname, productprice);
    }

    @Override
    public String toString() {
        return "Product{" +
                "productId=" + productId +
                ", productname='" + productname + '\'' +
                ", productprice=" + productprice +
                ", order=" + order +
                '}';
    }
}
