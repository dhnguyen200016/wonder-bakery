package wonder.wondercakecis3368project;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends CrudRepository<Product,Integer> {

    //Product findByProductname(String ProductName);

}
