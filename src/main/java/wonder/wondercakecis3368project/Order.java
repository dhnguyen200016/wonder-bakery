package wonder.wondercakecis3368project;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
public class Order implements Serializable {
    private int orderId;
    private String dateordered;
    private String orderstatus;
    private Integer quantityordered;
    private BigDecimal subtotal;
    private BigDecimal laborcharge;
    private BigDecimal tax;
    private BigDecimal total;

    private Employee employee;  //M-1 - Order to Employee
    private Customer customer;  //M-1 - Order to Customer
    private Set<Product> products = new HashSet<>();    //1-M - Order to Product

    ////////////////////////////////////////////
    public Order() {

    }

    /////////////////////////////////////////////////
    //1-M - Order to Product
    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    public Set<Product> getProducts(){return products;}

    public void setProducts(Set<Product> products) {
        this.products = products;
    }


    /////////////////////////////////////////////////
    //M-1 - Order to Customer
    @ManyToOne(cascade = CascadeType.PERSIST)
    public Customer getCustomer(){
        return customer;}

    public void setCustomer(Customer customer){
        this.customer = customer;
    }

    /////////////////////////////////////////////////
    //M-1 - Order to Customer
    @ManyToOne(cascade = CascadeType.PERSIST)
    public Employee getEmployee(){
        return employee;}

    public void setEmployee(Employee employee){
        this.employee = employee;
    }


    /////////////////////////////////////////////////
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "order_id", nullable = false)
    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    @Basic
    @Column(name = "dateordered", nullable = true, length = 50)
    public String getDateordered() {
        return dateordered;
    }

    public void setDateordered(String dateordered) {
        this.dateordered = dateordered;
    }

    @Basic
    @Column(name = "orderstatus", nullable = true, length = 50)
    public String getOrderstatus() {
        return orderstatus;
    }

    public void setOrderstatus(String orderstatus) {
        this.orderstatus = orderstatus;
    }

    @Basic
    @Column(name = "quantityordered", nullable = true)
    public Integer getQuantityordered() {
        return quantityordered;
    }

    public void setQuantityordered(Integer quantityordered) {
        this.quantityordered = quantityordered;
    }

    @Basic
    @Column(name = "subtotal", nullable = true, precision = 2)
    public BigDecimal getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(BigDecimal subtotal) {
        this.subtotal = subtotal;
    }

    @Basic
    @Column(name = "laborcharge", nullable = true, precision = 2)
    public BigDecimal getLaborcharge() {
        return laborcharge;
    }

    public void setLaborcharge(BigDecimal laborcharge) {
        this.laborcharge = laborcharge;
    }

    @Basic
    @Column(name = "tax", nullable = true, precision = 2)
    public BigDecimal getTax() {
        return tax;
    }

    public void setTax(BigDecimal tax) {
        this.tax = tax;
    }

    @Basic
    @Column(name = "total", nullable = true, precision = 2)
    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return orderId == order.orderId &&
                Objects.equals(dateordered, order.dateordered) &&
                Objects.equals(orderstatus, order.orderstatus) &&
                Objects.equals(quantityordered, order.quantityordered) &&
                Objects.equals(subtotal, order.subtotal) &&
                Objects.equals(laborcharge, order.laborcharge) &&
                Objects.equals(tax, order.tax) &&
                Objects.equals(total, order.total);
    }

    @Override
    public int hashCode() {
        return Objects.hash(orderId, dateordered, orderstatus, quantityordered, subtotal, laborcharge, tax, total);
    }

    /////////////////////////////////////////////////////////////////////////
    @Override
    public String toString() {
        return "Order{" +
                "orderId=" + orderId +
                ", dateordered='" + dateordered + '\'' +
                ", orderstatus='" + orderstatus + '\'' +
                ", quantityordered=" + quantityordered +
                ", subtotal=" + subtotal +
                ", laborcharge=" + laborcharge +
                ", tax=" + tax +
                ", total=" + total +
                ", employee=" + employee +
                '}';
    }

    /////////////////////////////////////////////////////////////////
    //methods
    public void addProduct(Product product){
        products.add(product);
        product.setOrder(this);
    }

    public void removeProduct(Product product){
        products.remove(product);
    }

    public void cancelOrder(){
        products.forEach(product -> product.setOrder(null));
        products.clear();
    }

}
