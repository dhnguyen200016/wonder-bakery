package wonder.wondercakecis3368project.controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;

import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import javafx.animation.Animation;
import javafx.animation.Interpolator;
import javafx.animation.Transition;
import javafx.application.Platform;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TitledPane;
import javafx.scene.control.ToggleButton;
import javafx.scene.input.*;
import javafx.scene.input.DataFormat;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import javafx.util.Callback;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import java.sql.*;
import java.util.List;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import wonder.wondercakecis3368project.*;

@Component
public class ReportController implements Initializable{

    Stage stage;
    Parent root;
    Scene scene;
    ///////////////////////////////////////////////////////////
    //
    final String DB_URL = "jdbc:postgresql://localhost:5433/wonder-cake";
    final String USER = "postgres";
    final String PASS = "postgres";

    Connection conn = null;
    PreparedStatement preparedStatement = null;

    ///////////////////////////////////
    @FXML
    private ToggleButton btnkanbanboard;
    @FXML
    private ToggleButton btnorder;
    @FXML
    public ToggleButton btncustomer;
    @FXML
    private ToggleButton btnemployee;
    @FXML
    private ToggleButton btnreport;
    @FXML
    private Button viewReportBTN;

    /////////////////////////////////////////////

    @FXML
    private ObservableList<ObservableList> data = FXCollections.observableArrayList();
    @FXML
    private TableView tableView;

    @FXML
    private ListView reportList;

//    @FXML
//    private TableView<Customer> tableView;

    private static final DataFormat REPORT_LIST = new DataFormat("cis3368/reportList");

    private Customer customer;
    private Employee employee;
    private wonder.wondercakecis3368project.Component component;
    private Order order;




    ////////////////////////////////////
//Added autowire
    @Autowired
    private ApplicationContext applicationContext;

    ////////////////////////////////////
    @Autowired
    private ComponentRepository componentRepository;
    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private ProductcomponentRepository productcomponentRepository;

    ////////////////////////////////////

    public ReportController() {

    }

    //////////////////////////////////////////////////////////////////


    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    @FXML
    public void gotoKanbanBoard() throws Exception{

        stage = (Stage)this.btnkanbanboard.getScene().getWindow();
        root = (Parent)FXMLLoader.load(this.getClass().getResource("/wonder/wondercakecis3368project/fxml/KanbanBoard.fxml"));
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    @FXML
    public void gotoOrder() throws Exception{

        stage = (Stage)this.btnorder.getScene().getWindow();
        root = (Parent)FXMLLoader.load(this.getClass().getResource("/wonder/wondercakecis3368project/fxml/Order.fxml"));
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    @FXML
    public void gotoCustomer() throws Exception {

        stage = (Stage)this.btncustomer.getScene().getWindow();
        root = (Parent)FXMLLoader.load(this.getClass().getResource("/wonder/wondercakecis3368project/fxml/Customer.fxml"));
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    @FXML
    public void gotoEmployee() throws Exception{

        stage = (Stage)this.btnemployee.getScene().getWindow();
        root = (Parent)FXMLLoader.load(this.getClass().getResource("/wonder/wondercakecis3368project/fxml/Employee.fxml"));
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    @FXML
    public void gotoReport() throws Exception{

        stage = (Stage)this.btnreport.getScene().getWindow();
        root = (Parent)FXMLLoader.load(this.getClass().getResource("/wonder/wondercakecis3368project/fxml/Report.fxml"));
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    /////////////////////////////////////////////////////////////////////////////////
    @FXML
    private void viewReport(ActionEvent event) throws IOException {

        if (event.getSource() == this.viewReportBTN) {
            tableView.getColumns().clear();
            tableView.getItems().clear();
            tableView.refresh();
        }

        preparedStatement = null;
        String SQL = null;

        SQL = "SELECT t.* FROM public.\"order\" t";

        try {

            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            System.out.println("Creating statement...");
            //////////////////////////////////////////////////////////////
            preparedStatement = conn.prepareStatement(SQL);
            System.out.println(SQL);
            ResultSet rs = preparedStatement.executeQuery();

            for (int i = 0; i < rs.getMetaData().getColumnCount(); i++) {
                //using non property style for making dynamic table
                final int j = i;
                TableColumn col = new TableColumn(rs.getMetaData().getColumnName(i + 1));
                col.setCellValueFactory((Callback<TableColumn.CellDataFeatures<ObservableList, String>, ObservableValue<String>>) param -> new SimpleStringProperty(param.getValue().get(j).toString()));

                tableView.getColumns().addAll(col);
                System.out.println("Column [" + i + "] ");
            }

            while (rs.next()) {
                //Iterate Row
                ObservableList<String> row = FXCollections.observableArrayList();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    //Iterate Column
                    row.add(rs.getString(i));
                }
                System.out.println("Row [1] added " + row);
                data.add(row);

            }

            tableView.setItems(data);
            conn.close();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error on Building Data");
        }

        /////////////////////////////////////////////////////
        //test method 2
//        BasicDataSource bs = new BasicDataSource();
//        bs.setUsername("postgres");
//        bs.setPassword("postgres");
//        bs.setUrl("jdbc:postgresql://localhost:5433/wonder-cake");


    }




    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * Called to initialize a controller after its root element has been
     * completely processed.
     *
     * @param location  The location used to resolve relative paths for the root object, or
     *                  {@code null} if the location is not known.
     * @param resources The resources used to localize the root object, or {@code null} if
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {

//        order = new Order();
//        order.setOrderId(3);
//        order.setDateordered("2018-11-02");
//        order.setOrderstatus("Pending");
//        order.setQuantityordered(1);
//        order.setTotal(BigDecimal.valueOf(7.94));
//        orderRepository.save(order);

//        customer = new Customer();
//        customer.setCustomerId(3);
//        customer.setFirstname("Johnny");
//        customer.setLastname("Cash");
//        customer.setOrganizationname("Art Associate");
//        customer.setPhone("556-897-8462");
//        customer.setEmail("Art@gmail.com");
//        customer.setStreetaddress("4567 Board Walk");
//        customer.setZipcode("77012");
//        customer.setCity("Houston");
//        customer.setState("Texas");
//        customer.setCountry("USA");
//        customerRepository.save(customer);

//        employee = new Employee();
//        employee.setEmployeeId(3);
//        employeeRepository.save(employee);

//        component = new wonder.wondercakecis3368project.Component();
//        component.setComponentname("Coffee Frosting");
//        component.setComponenttype("Frosting");
//        component.setComponentprice(BigDecimal.valueOf(2.00));
//        componentRepository.save(component);

        ////////////////////////////////////////////////
        //display data

//        reportList.getItems().add(customer);

//        for (Customer customerPrint : customerRepository.findAll())
//        {
//            System.out.println(customerPrint);
//        }

        /////////////////////////////////////////////////
    }




}
