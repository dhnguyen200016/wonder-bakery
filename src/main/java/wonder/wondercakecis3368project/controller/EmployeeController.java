package wonder.wondercakecis3368project.controller;

import java.io.IOException;
import java.net.URL;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import javafx.util.Callback;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

import wonder.wondercakecis3368project.Employee;
import wonder.wondercakecis3368project.EmployeeRepository;

@Component
public class EmployeeController implements Initializable{

    ///////////////////////////////////
    Stage stage;
    Parent root;
    Scene scene;

    ///////////////////////////////////////////////////////////
    //
    final String DB_URL = "jdbc:postgresql://localhost:5433/wonder-cake";
    final String USER = "postgres";
    final String PASS = "postgres";

    Connection conn = null;
    PreparedStatement preparedStatement = null;
    PreparedStatement preparedStatement_Add = null;
    PreparedStatement preparedStatement_Update = null;
    PreparedStatement preparedStatement_Delete = null;
    ///////////////////////////////////
    @FXML
    private ToggleButton btnkanbanboard;
    @FXML
    private ToggleButton btnorder;
    @FXML
    public ToggleButton btncustomer;
    @FXML
    private ToggleButton btnemployee;
    @FXML
    private ToggleButton btnreport;
    @FXML
    private Button viewReportBTN;
    @FXML
    private Button clearTextBTN;

    public Button btn_doAdd;

    public Button btn_doUpdate;

    public Button btn_doDelete;


    ////////////////////////////////////

    public TextField txt_id;
    public TextField txt_firstname;
    public TextField txt_lastname;
    public TextField txt_employeetitle;
    public TextField txt_phone;
    public TextField txt_email;
    public TextField txt_streetaddress;
    public TextField txt_zipcode;
    public TextField txt_city;
    public TextField txt_state;
    public TextField txt_country;


    /////////////////////////////////////////////

    @FXML
    private ObservableList<ObservableList> data = FXCollections.observableArrayList();
    @FXML
    private TableView tableView;

    ////////////////////////////////////
    @Autowired
    private EmployeeRepository employeeRepository;



    ////////////////////////////////////

    public EmployeeController() {

    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    @FXML
    public void gotoKanbanBoard() throws Exception{

        stage = (Stage)this.btnkanbanboard.getScene().getWindow();
        root = (Parent)FXMLLoader.load(this.getClass().getResource("/wonder/wondercakecis3368project/fxml/KanbanBoard.fxml"));
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    @FXML
    public void gotoOrder() throws Exception{

        stage = (Stage)this.btnorder.getScene().getWindow();
        root = (Parent)FXMLLoader.load(this.getClass().getResource("/wonder/wondercakecis3368project/fxml/Order.fxml"));
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    @FXML
    public void gotoCustomer() throws Exception {

        stage = (Stage)this.btncustomer.getScene().getWindow();
        root = (Parent)FXMLLoader.load(this.getClass().getResource("/wonder/wondercakecis3368project/fxml/Customer.fxml"));
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    @FXML
    public void gotoEmployee() throws Exception{

        stage = (Stage)this.btnemployee.getScene().getWindow();
        root = (Parent)FXMLLoader.load(this.getClass().getResource("/wonder/wondercakecis3368project/fxml/Employee.fxml"));
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    @FXML
    public void gotoReport() throws Exception{

        stage = (Stage)this.btnreport.getScene().getWindow();
        root = (Parent)FXMLLoader.load(this.getClass().getResource("/wonder/wondercakecis3368project/fxml/Report.fxml"));
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    @FXML
    private void viewEmployeeReport(ActionEvent event) throws IOException {

        if (event.getSource() == this.viewReportBTN) {
            tableView.getColumns().clear();
            tableView.getItems().clear();
            tableView.refresh();
        }

        preparedStatement = null;
        String SQL = null;

        SQL = "SELECT t.* FROM public.\"employee\" t";

        try {

            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            System.out.println("Creating statement...");
            //////////////////////////////////////////////////////////////
            preparedStatement = conn.prepareStatement(SQL);
            System.out.println(SQL);
            ResultSet rs = preparedStatement.executeQuery();

            for (int i = 0; i < rs.getMetaData().getColumnCount(); i++) {
                //using non property style for making dynamic table
                final int j = i;
                TableColumn col = new TableColumn(rs.getMetaData().getColumnName(i + 1));
                col.setCellValueFactory((Callback<TableColumn.CellDataFeatures<ObservableList, String>, ObservableValue<String>>) param -> new SimpleStringProperty(param.getValue().get(j).toString()));

                tableView.getColumns().addAll(col);
                System.out.println("Column [" + i + "] ");
            }

            while (rs.next()) {
                //Iterate Row
                ObservableList<String> row = FXCollections.observableArrayList();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    //Iterate Column
                    row.add(rs.getString(i));
                }
                System.out.println("Row [1] added " + row);
                data.add(row);

            }

            tableView.setItems(data);
            conn.close();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error on Building Data");
        }
    }


    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    /**
     * Called to initialize a controller after its root element has been
     * completely processed.
     *
     * @param location  The location used to resolve relative paths for the root object, or
     *                  {@code null} if the location is not known.
     * @param resources The resources used to localize the root object, or {@code null} if
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {



    }

    public void doAdd() {
        /**/
        preparedStatement_Add = null;
        String SQL_Add = null;

        String addemployeeid = txt_id.getText();
        String addfirstname = txt_firstname.getText();
        String addlastname = txt_lastname.getText();
        String addemployeetitle = txt_employeetitle.getText();
        String addphone = txt_phone.getText();
        String addemail = txt_email.getText();
        String addstreeaddress = txt_streetaddress.getText();
        String addzipcode = txt_zipcode.getText();
        String addcity = txt_city.getText();
        String addstate = txt_state.getText();
        String addcountry = txt_country.getText();

        SQL_Add = "INSERT INTO \"public\".\"employee\"" +
                " (\"employee_id\", \"firstname\", \"lastname\", \"employeetitle\", \"phone\", \"email\", \"streetaddress\", \"zipcode\", \"city\", \"state\", \"country\") " +
                "VALUES " +
                "(" +addemployeeid+ ", '" +addfirstname+ "', '" +addlastname+ "', '"+addemployeetitle+"', '"+addphone+"', '"+addemail+"', '"+addstreeaddress+"', '"+addzipcode+"', '"+addcity+"', '"+addstate+"', '"+addcountry+"')";

        try {

            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            System.out.println("Creating statement...");

            preparedStatement_Add = conn.prepareStatement(SQL_Add);

            System.out.println(SQL_Add);

            preparedStatement_Add.executeUpdate();

            System.out.println("Statement executed...");

            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public void doUpdate() {
        /**/


    }


    public void doDelete() {
        /**/
        preparedStatement_Delete = null;
        String SQL_Delete = null;
        String deleteemployee = null;

        deleteemployee = txt_id.getText();

        SQL_Delete = "DELETE FROM \"public\".\"employee\" WHERE \"employee_id\" = "+deleteemployee+"";


        try {

            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            System.out.println("Creating statement...");

            preparedStatement_Delete = conn.prepareStatement(SQL_Delete);

            System.out.println(SQL_Delete);

            preparedStatement_Delete.executeUpdate();

            System.out.println("Statement executed...");

            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void doClearText(){
        txt_id.setText("");
        txt_firstname.setText("");
        txt_lastname.setText("");
        txt_employeetitle.setText("");
        txt_phone.setText("");
        txt_email.setText("");
        txt_streetaddress.setText("");
        txt_zipcode.setText("");
        txt_city.setText("");
        txt_state.setText("");
        txt_country.setText("");
    }
}
