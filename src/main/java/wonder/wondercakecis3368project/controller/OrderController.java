package wonder.wondercakecis3368project.controller;

import java.io.IOException;
import java.net.URL;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import javafx.animation.Animation;
import javafx.animation.Interpolator;
import javafx.animation.Transition;
import javafx.application.Platform;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import javafx.util.Callback;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import wonder.wondercakecis3368project.*;

@Component
public class OrderController implements Initializable{

    ///////////////////////////////////
    Stage stage;
    Parent root;
    Scene scene;

    ///////////////////////////////////////////////////////////
    //
    final String DB_URL = "jdbc:postgresql://localhost:5433/wonder-cake";
    final String USER = "postgres";
    final String PASS = "postgres";

    Connection conn = null;

    PreparedStatement preparedStatementCustomer = null;
    PreparedStatement preparedStatementOrder = null;
    PreparedStatement preparedStatementProduct = null;
    PreparedStatement preparedStatementComponent = null;

    PreparedStatement preparedStatement_Add = null;
    PreparedStatement preparedStatement_Update = null;
    PreparedStatement preparedStatement_Delete = null;

    ///////////////////////////////////

    @FXML
    private ToggleButton btnkanbanboard;
    @FXML
    private ToggleButton btnorder;
    @FXML
    public ToggleButton btncustomer;
    @FXML
    private ToggleButton btnemployee;
    @FXML
    private ToggleButton btnreport;

    @FXML
    private Button viewReportBTN;
    @FXML
    private Button clearTextBTN;
    @FXML
    private Button calculateBTN;

    public Button btn_doAdd;

    public Button btn_doUpdate;

    public Button btn_doDelete;

    public TextField txt_customerid;
    public TextField txt_orderid;
    public TextField txt_productid;
    public TextField txt_productcomponentid;
    public TextField txt_componentid;
    public TextField txt_dateordered;
    public TextField txt_orderstatus;
    public TextField txt_quantityordered;
    public TextField txt_quantity;
    public TextField txt_subtotal;
    public TextField txt_laborcharge;
    public TextField txt_tax;
    public TextField txt_total;


    /////////////////////////////////////////////
    @FXML
    private ObservableList<ObservableList> dataCustomer = FXCollections.observableArrayList();
    @FXML
    private ObservableList<ObservableList> dataOrder = FXCollections.observableArrayList();
    @FXML
    private ObservableList<ObservableList> dataProduct = FXCollections.observableArrayList();
    @FXML
    private ObservableList<ObservableList> dataComponent = FXCollections.observableArrayList();
    @FXML
    private TableView tableViewCustomer;
    @FXML
    private TableView tableViewOrder;
    @FXML
    private TableView tableViewProduct;
    @FXML
    private TableView tableViewComponent;

    ////////////////////////////////////
//just added autowire
    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private ComponentRepository componentRepository;
    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private ProductcomponentRepository productcomponentRepository;

    ////////////////////////////////////

    public OrderController() {

    }



    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    @FXML
    public void gotoKanbanBoard() throws Exception{

        stage = (Stage)this.btnkanbanboard.getScene().getWindow();
        root = (Parent)FXMLLoader.load(this.getClass().getResource("/wonder/wondercakecis3368project/fxml/KanbanBoard.fxml"));
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    @FXML
    public void gotoOrder() throws Exception{

        stage = (Stage)this.btnorder.getScene().getWindow();
        root = (Parent)FXMLLoader.load(this.getClass().getResource("/wonder/wondercakecis3368project/fxml/Order.fxml"));
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    @FXML
    public void gotoCustomer() throws Exception {

        stage = (Stage)this.btncustomer.getScene().getWindow();
        root = (Parent)FXMLLoader.load(this.getClass().getResource("/wonder/wondercakecis3368project/fxml/Customer.fxml"));
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    @FXML
    public void gotoEmployee() throws Exception{

        stage = (Stage)this.btnemployee.getScene().getWindow();
        root = (Parent)FXMLLoader.load(this.getClass().getResource("/wonder/wondercakecis3368project/fxml/Employee.fxml"));
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    @FXML
    public void gotoReport() throws Exception{

        stage = (Stage)this.btnreport.getScene().getWindow();
        root = (Parent)FXMLLoader.load(this.getClass().getResource("/wonder/wondercakecis3368project/fxml/Report.fxml"));
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    @FXML
    private void viewAllReport(ActionEvent event) throws IOException {

        if (event.getSource() == this.viewReportBTN) {
            tableViewCustomer.getColumns().clear();
            tableViewOrder.getColumns().clear();
            tableViewProduct.getColumns().clear();
            tableViewComponent.getColumns().clear();

            tableViewCustomer.getItems().clear();
            tableViewOrder.getItems().clear();
            tableViewProduct.getItems().clear();
            tableViewComponent.getItems().clear();

            tableViewCustomer.refresh();
            tableViewOrder.refresh();
            tableViewProduct.refresh();
            tableViewComponent.refresh();

        }

        preparedStatementCustomer = null;
        preparedStatementOrder = null;
        preparedStatementProduct = null;
        preparedStatementComponent = null;

        String SQLcustomer = null;
        String SQLorder = null;
        String SQLproduct = null;
        String SQLcomponent = null;

        SQLcustomer = "SELECT t.* FROM public.\"customer\" t";
        SQLorder = "SELECT t.* FROM public.\"order\" t";
        SQLproduct = "SELECT t.* FROM public.\"product\" t";
        SQLcomponent = "SELECT t.* FROM public.\"component\" t";

        try {

            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            System.out.println("Creating statement...");
            //////////////////////////////////////////////////////////////
            //customer list
            preparedStatementCustomer = conn.prepareStatement(SQLcustomer);
            System.out.println(SQLcustomer);
            ResultSet rsCustomer = preparedStatementCustomer.executeQuery();

            for (int i = 0; i < rsCustomer.getMetaData().getColumnCount(); i++) {
                //using non property style for making dynamic table
                final int j = i;
                TableColumn col = new TableColumn(rsCustomer.getMetaData().getColumnName(i + 1));
                col.setCellValueFactory((Callback<TableColumn.CellDataFeatures<ObservableList, String>, ObservableValue<String>>) param -> new SimpleStringProperty(param.getValue().get(j).toString()));

                tableViewCustomer.getColumns().addAll(col);
                System.out.println("Column [" + i + "] ");
            }
            while (rsCustomer.next()) {
                //Iterate Row
                ObservableList<String> row = FXCollections.observableArrayList();
                for (int i = 1; i <= rsCustomer.getMetaData().getColumnCount(); i++) {
                    //Iterate Column
                    row.add(rsCustomer.getString(i));
                }
                System.out.println("Row [1] added " + row);
                dataCustomer.add(row);

            }
            tableViewCustomer.setItems(dataCustomer);

            //////////////////////////////////////////////////////////////
            //order list
            preparedStatementOrder = conn.prepareStatement(SQLorder);
            System.out.println(SQLorder);
            ResultSet rsOrder = preparedStatementOrder.executeQuery();

            for (int i = 0; i < rsOrder.getMetaData().getColumnCount(); i++) {
                //using non property style for making dynamic table
                final int j = i;
                TableColumn col = new TableColumn(rsOrder.getMetaData().getColumnName(i + 1));
                col.setCellValueFactory((Callback<TableColumn.CellDataFeatures<ObservableList, String>, ObservableValue<String>>) param -> new SimpleStringProperty(param.getValue().get(j).toString()));

                tableViewOrder.getColumns().addAll(col);
                System.out.println("Column [" + i + "] ");
            }
            while (rsOrder.next()) {
                //Iterate Row
                ObservableList<String> row = FXCollections.observableArrayList();
                for (int i = 1; i <= rsOrder.getMetaData().getColumnCount(); i++) {
                    //Iterate Column
                    row.add(rsOrder.getString(i));
                }
                System.out.println("Row [1] added " + row);
                dataOrder.add(row);

            }
            tableViewOrder.setItems(dataOrder);

            //////////////////////////////////////////////////////////////
            //product list
            preparedStatementProduct = conn.prepareStatement(SQLproduct);
            System.out.println(SQLproduct);
            ResultSet rsProduct = preparedStatementProduct.executeQuery();

            for (int i = 0; i < rsProduct.getMetaData().getColumnCount(); i++) {
                //using non property style for making dynamic table
                final int j = i;
                TableColumn col = new TableColumn(rsProduct.getMetaData().getColumnName(i + 1));
                col.setCellValueFactory((Callback<TableColumn.CellDataFeatures<ObservableList, String>, ObservableValue<String>>) param -> new SimpleStringProperty(param.getValue().get(j).toString()));

                tableViewProduct.getColumns().addAll(col);
                System.out.println("Column [" + i + "] ");
            }
            while (rsProduct.next()) {
                //Iterate Row
                ObservableList<String> row = FXCollections.observableArrayList();
                for (int i = 1; i <= rsProduct.getMetaData().getColumnCount(); i++) {
                    //Iterate Column
                    row.add(rsProduct.getString(i));
                }
                System.out.println("Row [1] added " + row);
                dataProduct.add(row);

            }
            tableViewProduct.setItems(dataProduct);

            //////////////////////////////////////////////////////////////
            //component list
            preparedStatementComponent = conn.prepareStatement(SQLcomponent);
            System.out.println(SQLcomponent);
            ResultSet rsComponent = preparedStatementComponent.executeQuery();

            for (int i = 0; i < rsComponent.getMetaData().getColumnCount(); i++) {
                //using non property style for making dynamic table
                final int j = i;
                TableColumn col = new TableColumn(rsComponent.getMetaData().getColumnName(i + 1));
                col.setCellValueFactory((Callback<TableColumn.CellDataFeatures<ObservableList, String>, ObservableValue<String>>) param -> new SimpleStringProperty(param.getValue().get(j).toString()));

                tableViewComponent.getColumns().addAll(col);
                System.out.println("Column [" + i + "] ");
            }
            while (rsComponent.next()) {
                //Iterate Row
                ObservableList<String> row = FXCollections.observableArrayList();
                for (int i = 1; i <= rsComponent.getMetaData().getColumnCount(); i++) {
                    //Iterate Column
                    row.add(rsComponent.getString(i));
                }
                System.out.println("Row [1] added " + row);
                dataComponent.add(row);

            }
            tableViewComponent.setItems(dataComponent);


            //////////////////////////////////////////////////////////////
            //end of loading
            conn.close();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error on Building Data");
        }
    }//end of listings






    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * Called to initialize a controller after its root element has been
     * completely processed.
     *
     * @param location  The location used to resolve relative paths for the root object, or
     *                  {@code null} if the location is not known.
     * @param resources The resources used to localize the root object, or {@code null} if
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////


    public void doAdd() {
        /**/
//        preparedStatement_Add = null;
//        String SQL_Add = null;
//
//
//        SQL_Add = "";
//
//        try {
//
//            conn = DriverManager.getConnection(DB_URL, USER, PASS);
//            System.out.println("Creating statement...");
//
//            preparedStatement_Add = conn.prepareStatement(SQL_Add);
//
//            System.out.println(SQL_Add);
//
//            preparedStatement_Add.executeUpdate();
//
//            System.out.println("Statement executed...");
//
//            conn.close();
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
    }


    public void doUpdate() {
        /**/


    }


    public void doDelete() {
        /**/
/*
        preparedStatement_Delete = null;
        String SQL_Delete = null;

        SQL_Delete = "";


        try {

            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            System.out.println("Creating statement...");

            preparedStatement_Delete = conn.prepareStatement(SQL_Delete);

            System.out.println(SQL_Delete);

            preparedStatement_Delete.executeUpdate();

            System.out.println("Statement executed...");

            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
*/
    }


}
