package wonder.wondercakecis3368project.controller;

import java.io.IOException;
import java.net.URL;
import javafx.scene.control.TableView;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.animation.Animation;
import javafx.animation.Interpolator;
import javafx.animation.Transition;
import javafx.application.Platform;

import javafx.event.ActionEvent;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TitledPane;
import javafx.scene.control.ToggleButton;
import javafx.scene.input.DataFormat;
import javafx.scene.input.MouseEvent;
import javafx.event.ActionEvent;
import javafx.scene.control.ListView;
import javafx.scene.input.*;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import java.util.ArrayList;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import wonder.wondercakecis3368project.*;

@Component
public class KanbanBoardController{

    Stage stage;
    Parent root;
    Scene scene;

    ///////////////////////////////////
    @FXML
    private ToggleButton btnkanbanboard;
    @FXML
    private ToggleButton btnorder;
    @FXML
    public ToggleButton btncustomer;
    @FXML
    private ToggleButton btnemployee;
    @FXML
    private ToggleButton btnreport;
    @FXML
    private  ListView preproductionlist;
    @FXML
    private ListView productionlist;
    @FXML
    private ListView closelist;
    @FXML
    private ListView archivelist;

    ////////////////////////////////////
//Autowire added
    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private ProductcomponentRepository productcomponentRepository;

    ////////////////////////////////////

    public KanbanBoardController() {

    }
    private Component component;
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    @FXML
    public void gotoKanbanBoard() throws Exception{

        stage = (Stage)this.btnkanbanboard.getScene().getWindow();
        root = (Parent)FXMLLoader.load(this.getClass().getResource("/wonder/wondercakecis3368project/fxml/KanbanBoard.fxml"));
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    @FXML
    public void gotoOrder() throws Exception{

        stage = (Stage)this.btnorder.getScene().getWindow();
        root = (Parent)FXMLLoader.load(this.getClass().getResource("/wonder/wondercakecis3368project/fxml/Order.fxml"));
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    @FXML
    public void gotoCustomer() throws Exception {

        stage = (Stage)this.btncustomer.getScene().getWindow();
        root = (Parent)FXMLLoader.load(this.getClass().getResource("/wonder/wondercakecis3368project/fxml/Customer.fxml"));
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    @FXML
    public void gotoEmployee() throws Exception{

        stage = (Stage)this.btnemployee.getScene().getWindow();
        root = (Parent)FXMLLoader.load(this.getClass().getResource("/wonder/wondercakecis3368project/fxml/Employee.fxml"));
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    @FXML
    public void gotoReport() throws Exception{

        stage = (Stage)this.btnreport.getScene().getWindow();
        root = (Parent)FXMLLoader.load(this.getClass().getResource("/wonder/wondercakecis3368project/fxml/Report.fxml"));
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////

    private static final DataFormat productLIST = new DataFormat("productcomponentList");


    public void dragDetected (MouseEvent mouseEvent){

//        int selectItem = productLIST.getSelectionModel().getSelectedIndices().size();
//        System.out.println(String.format("%d selected", selectItem));
//        if(selectItem > 0) {
//            Dragboard dragboard = productLIST.startDragAndDrop(TransferMode.MOVE);
//            ArrayList<Productcomponent> selectedcomponents = new ArrayList<>(productLIST.getSelectionModel().getSelectedItems());
//            ClipboardContent clipboardContent = new ClipboardContent();
//            clipboardContent.put(productLIST, selectedcomponents);
//            dragboard.setContent(clipboardContent);
//            mouseEvent.consume();
//        } else{
//            System.out.println("Nothing Selected");
//            mouseEvent.consume();
//        }
    }
    public void dragDone (DragEvent dragEvent){

        System.out.println("Drag Done");
//        TransferMode transferMode = dragEvent.getAcceptedTransferMode();
//        if(transferMode == TransferMode.MOVE){
//            removeItems();
//        }
//        dragEvent.consume();
//
//        productcomponentRepository.findAll().forEach(System.out::println);

    }

    public void dragDropped (DragEvent dragEvent){

//        boolean dragFinished = false;
//        Dragboard dragboard = dragEvent.getDragboard();
//        if(dragboard.hasContent(productLIST)){
//            ArrayList<Productcomponent> productcomponents = (ArrayList<Productcomponent>) dragboard.getContent(productLIST);
//            productcomponents.forEach(productcomponent -> {
//                component.addProductComponent(productcomponent);
//            });
//            ComponentRepository.save(component);
//            preproductionlist.getItems().addAll(productcomponents);
//            dragFinished = true;
//        }
//        dragEvent.setDropCompleted(dragFinished);
//        dragEvent.consume();
    }

    public void dragOver (DragEvent dragEvent){
        Dragboard dragboard = dragEvent.getDragboard();
        if (dragboard.hasContent(productLIST));{
            dragEvent.acceptTransferModes(TransferMode.MOVE);
        }
        dragEvent.consume();
    }
/////////////////////////////////////////////////////////////////////

    private static final DataFormat finalproductLIST = new DataFormat("productList");


    public void productdragDetected (MouseEvent mouseEvent){

//        int selectItem = finalproductLIST.getSelectionModel().getSelectedIndices().size();
//        System.out.println(String.format("%d selected", selectItem));
//        if(selectItem > 0) {
//            Dragboard dragboard = finalproductLIST.startDragAndDrop(TransferMode.MOVE);
//            ArrayList<Product> selectedproduct = new ArrayList<>(finalproductLIST.getSelectionModel().getSelectedItems());
//            ClipboardContent clipboardContent = new ClipboardContent();
//            clipboardContent.put(finalproductLIST, selectedcomponents);
//            dragboard.setContent(clipboardContent);
//            mouseEvent.consume();
//        } else{
//            System.out.println("Nothing Selected");
//            mouseEvent.consume();
//        }
    }
    public void productdragDone (DragEvent dragEvent){

        System.out.println("Drag Done");
//        TransferMode transferMode = dragEvent.getAcceptedTransferMode();
//        if(transferMode == TransferMode.MOVE){
//            removeItems();
//        }
//        dragEvent.consume();
//
//        ProductRepository.findAll().forEach(System.out::println);

    }

    public void productdragDropped (DragEvent dragEvent){

//        boolean dragFinished = false;
//        Dragboard dragboard = dragEvent.getDragboard();
//        if(dragboard.hasContent(productLIST)){
//            ArrayList<Productcomponent> product = (ArrayList<Productcomponent>) dragboard.getContent(finalproductLIST);
//            product.forEach(productcomponent -> {
//                component.addProduct();
//            });
//            ComponentRepository.save(component);
//            productionlist.getItems().addAll(product);
//            dragFinished = true;
//        }
//        dragEvent.setDropCompleted(dragFinished);
//        dragEvent.consume();
    }

    public void productdragOver (DragEvent dragEvent){
        Dragboard dragboard = dragEvent.getDragboard();
        if (dragboard.hasContent(finalproductLIST));{
            dragEvent.acceptTransferModes(TransferMode.MOVE);
        }
        dragEvent.consume();
    }
}