package wonder.wondercakecis3368project;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public interface ProductcomponentRepository extends CrudRepository<Productcomponent,Integer> {

    //ArrayList<Product> findAllByProductId(int Product_ID);
    //ArrayList<Component> findAllByComponentId(int Component_ID);

}
