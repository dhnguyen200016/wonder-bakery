package wonder.wondercakecis3368project;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import wonder.wondercakecis3368project.Employee;

import java.util.ArrayList;

@Repository
public interface EmployeeRepository extends CrudRepository<Employee,Integer>{

   // Employee findByFirstname(String EmpFirstName);

}
