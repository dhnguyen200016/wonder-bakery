package wonder.wondercakecis3368project;

import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
public class Employee {
    private int employeeId;
    private String firstname;
    private String lastname;
    private String employeetitle;
    private String phone;
    private String email;
    private String streetaddress;
    private String zipcode;
    private String city;
    private String state;
    private String country;

    private Set<Order> orders = new HashSet<>();

    ////////////////////////////////////
    @Autowired
    private ComponentRepository componentRepository;
    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private ProductcomponentRepository productcomponentRepository;

    /////////////////////////////////////////////////
    public Employee() {

    }

    /////////////////////////////////////////////////
    //one to many - Employee to Order
    @OneToMany(mappedBy = "employee", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    public Set<Order> getOrders(){return orders;}

    public void setOrders(Set<Order> orders) {
        this.orders = orders;
    }

    /////////////////////////////////////////////////

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "employee_id", nullable = false)
    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    @Basic
    @Column(name = "firstname", nullable = true, length = 50)
    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    @Basic
    @Column(name = "lastname", nullable = true, length = 50)
    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    @Basic
    @Column(name = "employeetitle", nullable = true, length = 50)
    public String getEmployeetitle() {
        return employeetitle;
    }

    public void setEmployeetitle(String employeetitle) {
        this.employeetitle = employeetitle;
    }

    @Basic
    @Column(name = "phone", nullable = true, length = 26)
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Basic
    @Column(name = "email", nullable = true, length = 55)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "streetaddress", nullable = true, length = 100)
    public String getStreetaddress() {
        return streetaddress;
    }

    public void setStreetaddress(String streetaddress) {
        this.streetaddress = streetaddress;
    }

    @Basic
    @Column(name = "zipcode", nullable = true, length = 12)
    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    @Basic
    @Column(name = "city", nullable = true, length = 60)
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Basic
    @Column(name = "state", nullable = true, length = 55)
    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Basic
    @Column(name = "country", nullable = true, length = 55)
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return employeeId == employee.employeeId &&
                Objects.equals(firstname, employee.firstname) &&
                Objects.equals(lastname, employee.lastname) &&
                Objects.equals(employeetitle, employee.employeetitle) &&
                Objects.equals(phone, employee.phone) &&
                Objects.equals(email, employee.email) &&
                Objects.equals(streetaddress, employee.streetaddress) &&
                Objects.equals(zipcode, employee.zipcode) &&
                Objects.equals(city, employee.city) &&
                Objects.equals(state, employee.state) &&
                Objects.equals(country, employee.country);
    }

    @Override
    public int hashCode() {
        return Objects.hash(employeeId, firstname, lastname, employeetitle, phone, email, streetaddress, zipcode, city, state, country);
    }
    ///////////////////////////////////////////////////////////////////

    @Override
    public String toString() {
        return "Employee{" +
                "employeeId=" + employeeId +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", employeetitle='" + employeetitle + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", streetaddress='" + streetaddress + '\'' +
                ", zipcode='" + zipcode + '\'' +
                ", city='" + city + '\'' +
                ", state='" + state + '\'' +
                ", country='" + country + '\'' +
                '}';
    }

    public void addOrder(Order order){
        orders.add(order);
        order.setEmployee(this);
    }


}
