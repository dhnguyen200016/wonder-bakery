package wonder.wondercakecis3368project;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ComponentRepository extends CrudRepository<Component,Integer> {

    //Component findByComponentname (String ComponentName);
}
