package wonder.wondercakecis3368project;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
public class Component {
    private int componentId;
    private String componentname;
    private String componenttype;
    private BigDecimal componentprice;

    private Set<Productcomponent> productcomponents = new HashSet<>();

    //////////////////////////////////////////
    public Component() {
    }

    /////////////////////////////////////////
    //one to many - Component to ProductComponent
    @OneToMany(mappedBy = "component", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    public Set<Productcomponent> getProductcomponents(){return productcomponents;}

    public void setProductcomponents(Set<Productcomponent> productcomponents) {
        this.productcomponents = productcomponents;
    }


    /////////////////////////////////////////
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "component_id", nullable = false)
    public int getComponentId() {
        return componentId;
    }

    public void setComponentId(int componentId) {
        this.componentId = componentId;
    }

    @Basic
    @Column(name = "componentname", nullable = true, length = 50)
    public String getComponentname() {
        return componentname;
    }

    public void setComponentname(String componentname) {
        this.componentname = componentname;
    }

    @Basic
    @Column(name = "componenttype", nullable = true, length = 50)
    public String getComponenttype() {
        return componenttype;
    }

    public void setComponenttype(String componenttype) {
        this.componenttype = componenttype;
    }

    @Basic
    @Column(name = "componentprice", nullable = true, precision = 2)
    public BigDecimal getComponentprice() {
        return componentprice;
    }

    public void setComponentprice(BigDecimal componentprice) {
        this.componentprice = componentprice;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Component component = (Component) o;
        return componentId == component.componentId &&
                Objects.equals(componentname, component.componentname) &&
                Objects.equals(componenttype, component.componenttype) &&
                Objects.equals(componentprice, component.componentprice);
    }

    @Override
    public int hashCode() {
        return Objects.hash(componentId, componentname, componenttype, componentprice);
    }

    @Override
    public String toString() {
        return "Component{" +
                "componentId=" + componentId +
                ", componentname='" + componentname + '\'' +
                ", componenttype='" + componenttype + '\'' +
                ", componentprice=" + componentprice +
                '}';
    }
}
