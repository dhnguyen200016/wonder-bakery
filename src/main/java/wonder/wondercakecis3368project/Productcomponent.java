package wonder.wondercakecis3368project;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
public class Productcomponent implements Serializable {

    private int productcomponentId;
    private Integer quantity;

    private Product product;    //M-1 - ProductComponent to Product
    private Component component;    //M-1 - ProductComponent to Component

    ////////////////////////////////////////////
    public Productcomponent() {

    }

    ////////////////////////////////////////////
    //M-1 - ProductComponent to Product
    @ManyToOne(cascade = CascadeType.PERSIST)
    public Product getProduct(){
        return product;}

    public void setProduct(Product product){
        this.product = product;
    }

    ////////////////////////////////////////////
    //M-1 - ProductComponent to Component
    @ManyToOne(cascade = CascadeType.PERSIST)
    public Component getComponent(){
        return component;}

    public void setComponent(Component component){
        this.component = component;
    }

    ////////////////////////////////////////////
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "productcomponent_id", nullable = false)
    public int getProductcomponentId() {
        return productcomponentId;
    }

    public void setProductcomponentId(int productcomponentId) {
        this.productcomponentId = productcomponentId;
    }

    @Basic
    @Column(name = "quantity", nullable = true)
    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Productcomponent that = (Productcomponent) o;
        return productcomponentId == that.productcomponentId &&
                Objects.equals(quantity, that.quantity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(productcomponentId, quantity);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public String toString() {
        return "Productcomponent{" +
                "productcomponentId=" + productcomponentId +
                ", quantity=" + quantity +
                ", product=" + product +
                ", component=" + component +
                '}';
    }

    ///////////////////////////////////////////////////////



}
